const faker = require('faker');
const maxPages = 1000;
const maxTagsOnPage = 10;

const pageFactory = async (knex) => {
  let title = faker.lorem.sentence();
  return {
    "title" : title,
    "meta_title" : title,
    "slug" : title.toLowerCase().replace(/\s+/g, '-').replace('.', ''),
    "header_img" : `https://i.picsum.photos/id/${faker.random.number(1000)}/640/480.jpg`,
    "user_id" : (await knex('users').orderByRaw('RAND()').limit(1))[0].id || 1, 
    "tags" : faker.random.boolean() ? null : JSON.stringify(
      (await knex('tags').orderByRaw('RAND()').limit(faker.random.number(maxTagsOnPage))).map(obj => obj.id)
    ),
  }
};

exports.pageFactory = pageFactory;

exports.seed = async function (knex) {
    //   Deletes ALL existing entries
    await knex('pages').del()
    await knex.raw('ALTER TABLE pages AUTO_INCREMENT = 1')

    // Inserting
    let pages = await Promise.all(Array(maxPages).fill({}).map(async obj => await pageFactory(knex)));
    await knex('pages').insert(pages);

    console.log(`Inserted ${maxPages} rows page table`)
    return true;
};
