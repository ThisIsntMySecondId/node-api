const faker = require('faker');
const bcrypt = require('bcrypt');
const bcryptSaltRounds = 10;

const maxApiUsers = 100;

const apiUserFactory = async (knex) => {
    const name = faker.name.findName();
    const apiKeyPrefix = faker.internet.password(8);
    const apiKey = faker.internet.password(16).replace(/[\W_]+/g,'');
    const apiKeyHash = await bcrypt.hash(apiKey, bcryptSaltRounds);
    return {
        "name": name,
        "email": faker.internet.email(name),
        "password": faker.internet.password(8),
        "recovery_email": faker.internet.email(),
        "api_key": apiKey,
        "api_key_prefix": apiKeyPrefix,
        "api_key_hash": apiKeyHash,
        "api_call_limit": 1000,
        "api_call_count": faker.random.number(1000),
        "user_settings": null,
    }
};

exports.apiUserFactory = apiUserFactory;

exports.seed = async function (knex) {
    // Deletes ALL existing entries and start from 1
    if(!(process.argv[4] === 'overwrite')){
        await knex('api_users').del()
        await knex.raw('ALTER TABLE api_users AUTO_INCREMENT = 1')
    }
    
    // inserting initial api_users
    let apiUsers = await Promise.all(Array(maxApiUsers).fill({}).map(async obj => await apiUserFactory(knex)));
    await knex('api_users').insert(apiUsers);

    console.log(`Inserted ${maxApiUsers} rows in api_user table`)
    return true;
};
