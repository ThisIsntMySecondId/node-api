const faker = require('faker');
const maxActions = 1000;
availableActions = ['like','love','enjoy','sad','angry','follow','share','upvote','downvote'];
availableTargets = ['users','posts','pages','comments'];

const actionFactory = async (knex) => {
    let action = faker.random.arrayElement(availableActions);
    let target = faker.random.arrayElement(availableTargets);
    let targetId = (await knex(target).orderByRaw('RAND()').limit(1))[0].id || 1;
    return {
        "user_id": (await knex('users').orderByRaw('RAND()').limit(1))[0].id || 1,
        "target_id": targetId,
        "target_type": target,
        "action": action,
    }
};

exports.actionFactory = actionFactory;

exports.seed = async function (knex) {
    // Deletes ALL existing entries and start from 1
    if(!(process.argv[4] === 'overwrite')){
        await knex('actions').del()
        await knex.raw('ALTER TABLE actions AUTO_INCREMENT = 1')
    }

    // inserting initial actions
    let initialActions = await Promise.all(Array(maxActions).fill({}).map(async obj => await actionFactory(knex)));
    await knex('actions').insert(initialActions);

    console.log(`Inserted ${maxActions} rows in action table`)
    return true;
};
