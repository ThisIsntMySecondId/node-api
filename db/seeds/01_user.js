const faker = require('faker');
const amount = 1000;
const profileThemes = ["light", "dark", "roses", "ocean", "desert", "alpinee"];

const userProfileFactory = () => {
  return {
    "bio": faker.lorem.paragraph().substring(0, 200),
    "avatar": faker.image.avatar(),
    "timeline": Array(faker.random.number(10)).fill({}).map(obj => ({
      "title": faker.lorem.sentence(),
      "description": faker.lorem.paragraph(3),
      "date": faker.date.past(faker.random.number(20)),
      "containsImage": faker.random.boolean()
    })),
    "theme": faker.random.arrayElement(profileThemes),
    "email_notifications": {
      "marketing": faker.random.boolean(),
      "feeds": faker.random.boolean(),
      "accountRelated": faker.random.boolean()
    }
  }
}

const userFactory = () => {
  let name = faker.name.findName();
  return {
    "name": name,
    "username": faker.internet.userName(name),
    "email": faker.internet.email(name),
    "mobile": faker.phone.phoneNumber('##########'),
    "region": faker.address.country(),
    "is_active": faker.random.boolean(),
    "profile": JSON.stringify(userProfileFactory()),
    "last_login_at": faker.random.boolean() ? faker.date.recent(30) : faker.date.past(2)
  }
};

exports.userFactory = userFactory;

exports.seed = async function (knex) {
  let users = Array(amount).fill({}).map(obj => userFactory());
  // Deletes ALL existing entries and start from 1
  await knex('users').del()
  await knex.raw('ALTER TABLE users AUTO_INCREMENT = 1')

  // Insert user
  if(amount <= 1000)
    await knex('users').insert(users);
  else {
    users = Array(parseInt(users.length/1000) + 1).fill().map((_, i) => {
        return users.slice(1000*i, 1000*i+1000)
    })
    await Promise.all(Array(amount/1000).fill().map(async (obj, index) => await knex('users').insert(users[index])))
  }
  console.log(`Inserted ${amount} rows in users table`)
  return true;
};
