const faker = require('faker');
const maxCategories = 300;
const depth = 5;

const categoryFactory = async (knex, haveParent = false) => {
    return {
        "parent_id" : haveParent && (await knex('categories').orderByRaw('RAND()').limit(1))[0].id || null,
        "title" : faker.company.catchPhraseNoun(),
        "description" : faker.lorem.sentence(),
    }
};

exports.categoryFactory = categoryFactory;

exports.seed = async function (knex) {
    // Deletes ALL existing entries
    await knex('categories').del()
    await knex.raw('ALTER TABLE categories AUTO_INCREMENT = 1')

    // Inserting
    let initialCategories = await Promise.all(Array(maxCategories/depth).fill({}).map(async obj =>await categoryFactory(knex)));
    await knex('categories').insert(initialCategories);

    // Set children comments
    for (let i = 0; i < depth - 1; i++) {
        let childCategories = await Promise.all(Array(maxCategories / depth).fill({}).map(async obj => await categoryFactory(knex, true)));
        await knex('categories').insert(childCategories);
    }

    console.log(`Inserted ${maxCategories} rows in category table`)
    return true;
};
