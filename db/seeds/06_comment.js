const faker = require('faker');
const maxComments = 1000;
const maxDepth = 5;

const commentFactory = async (knex, haveParent = false) => {
    let parentComment = (await knex('comments').orderByRaw('RAND()').limit(1))[0]
    return {
        "parent_id": haveParent && parentComment && parentComment.id || null,
        "user_id": (await knex('users').orderByRaw('RAND()').limit(1))[0].id || 1,
        "comment": faker.lorem.sentence(),
        "post_id": (await knex('posts').orderByRaw('RAND()').limit(1))[0].id || 1,
    }
};

exports.commentFactory = commentFactory;

exports.seed = async function (knex) {
    // Deletes ALL existing entries and start from 1
    await knex('comments').del()
    await knex.raw('ALTER TABLE comments AUTO_INCREMENT = 1')

    // inserting initial comments
    let initialComments = await Promise.all(Array(maxComments / maxDepth).fill({}).map(async obj => await commentFactory(knex)));
    await knex('comments').insert(initialComments);

    // inserting children comments
    for (let i = 0; i < maxDepth - 1; i++) {
        let childrenComments = await Promise.all(Array(maxComments / maxDepth).fill({}).map(async obj => await commentFactory(knex, true)));
        await knex('comments').insert(childrenComments);
    }

    console.log(`Inserted ${maxComments} rows in comment table`)
    return true;
};
