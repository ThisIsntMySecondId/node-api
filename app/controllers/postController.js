const postModel = require('../models/postModel')

module.exports.postlist = async function (req, res) {
    try {
        //  prepare filters, paginate, limit, search and sort query esp filters

        // Get post list from db
        const postList = await postModel.postlist(req.query)

        // Perform record manuplation on return object according to post setting
        // console.log(req.apiUser)

        // perform transformation according to post setting

        // return post list
        res.json({
            routeName: 'post list',
            post: req.post,
            data: postList
        });
    } catch (err) {
        return res.status(500).json({
            message: err
        })
    }
}
module.exports.post = async function (req, res) {
    try {
        // Get post list from db
        const post = await postModel.post(req.params.postId);

        if (!post.length) return res.status(401).json({ message: 'User Not Found' })

        // Perform manuplation on return object according to post setting

        // perform transformation according to post setting

        // return post list
        res.json({
            routeName: 'post info',
            post: req.post,
            data: post
        });
    } catch (err) {
        return res.status(500).json(err);
    }
}

// function to return post's followers
// function to return post's followings
// function to return post's shares
// function to return post's reactions
// function to return post's posts
// function to return post's pages
// function to return post's tags