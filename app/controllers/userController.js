const userModel = require('../models/userModel')

// TODO: do something about user profile getting returned as string

module.exports.userlist = async function (req, res) {
    try {
        //  prepare filters, paginate, limit, search and sort query esp filters

        // Get user list from db
        const userList = await userModel.userlist(req.query)

        // Perform record manuplation on return object according to user setting
        console.log(req.apiUser)

        // perform transformation according to user setting

        // return user list
        res.json({
            routeName: 'user list',
            user: req.user,
            data: userList
        });

    } catch (err) {
        return res.status(500).json({
            message: err
        })
    }
}
module.exports.user = async function (req, res) {
    try {
        // Get user list from db
        const user = await userModel.user(req.params.userId);

        if (!user.length) return res.status(401).json({ message: 'User Not Found' })

        // Perform manuplation on return object according to user setting

        // perform transformation according to user setting

        // return user list
        res.json({
            routeName: 'user info',
            user: req.user,
            data: user
        });
    } catch (err) {
        return res.status(500).json(err);
    }
}

// function to return user's followers
// function to return user's followings
// function to return user's shares
// function to return user's reactions
// function to return user's posts
// function to return user's pages
// function to return user's tags