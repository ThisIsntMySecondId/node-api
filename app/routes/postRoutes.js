let postRouter = require('express').Router();
let postController = require('../controllers/postController')

// /posts
postRouter.get('/', postController.postlist)

// /posts/id
postRouter.get('/:postId', postController.post)

// /posts/id/followers      ❎ 1️⃣ instead use of multiple endpoints, use single endpoint for all actions
// /posts/id/reactions      ❎ 1️⃣ instead use of multiple endpoints, use single endpoint for all actions
// /posts/id/shares         ❎ 1️⃣ instead use of multiple endpoints, use single endpoint for all actions
// /posts/id/user           ❎ 1️⃣ already in post info 2️⃣ can be controlled by api key settings
// /posts/id/tags           ❎ 1️⃣ already in post info 2️⃣ can be controlled by api key settings
// /posts/id/tagged-users   ❎ 1️⃣ already in post info 2️⃣ can be controlled by api key settings

// /posts/id/actions

module.exports = postRouter;
