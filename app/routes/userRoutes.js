let userRouter = require('express').Router();
let userController = require('../controllers/userController')

userRouter.get('/', userController.userlist)
userRouter.get('/:userId', userController.user)

// /user/follows
// /user/followers
// /user/reactions
// /user/shares
// /user/posts
// /user/pages
// /user/tags

module.exports = userRouter;
