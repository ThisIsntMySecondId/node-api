let express = require('express');
let router = express.Router();

let authMiddleware = require('../middlewares/apiAuth.js')
let userRoutes = require('./userRoutes.js')
let postRoutes = require('./postRoutes.js')
let pageRoutes = require('./pageRoutes.js')
let tagRoutes = require('./tagRoutes.js')
let categoryRoutes = require('./categoryRoutes.js')

/* GET home page. */
router.use('/api', authMiddleware);
router.use('/api/users', userRoutes);
router.use('/api/posts', postRoutes);
router.use('/api/pages', pageRoutes);
router.use('/api/tags', tagRoutes);
router.use('/api/categories', categoryRoutes);

module.exports = router;
