let userRouter = require('express').Router();

userRouter.get('/', function(req, res) {
    res.json({
        routeName: 'category list',
        user: req.user
    });
})

module.exports = userRouter;
