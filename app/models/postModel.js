const db = require('../../db/knex')
const searchableFields = ['title', 'slug', 'content']

module.exports.postlist = async function (filters) {
    console.log("filters", filters)
    console.log('inside post list')
    if(filters.fields) filters.fields = filters.fields.split(',')
    const { fields = [], page = 0, limit = 10, filteringConditions = [], q:search = '', sort = 'id' } = filters;
    const offset = (page && page > 0 ? page - 1 : 0) * limit
    const sortOrder = sort[0] === '-' ? [sort.slice(1), 'desc'] : [sort, 'asc']
    
    return await db('posts')
        .select(fields)
        .limit(limit)
        .offset(offset)
        .where(builder => {
            if(search) {
                searchableFields.forEach(fieldName => {
                    builder.orWhere(fieldName, 'like', `%${search}%`)
                })
            }
        })
        // filters not working atm
        .where(builder => {
            filteringConditions.forEach(condition => {
                builder.where(...condition)
            });
        })
        .orderBy(...sortOrder);
}

module.exports.post = async function (id) {
    console.log('inside post info')
    return await db('posts').where('id', id);
}