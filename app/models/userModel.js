const knex = require('../../db/knex')
const searchableFields = ['name', 'email', 'username']

module.exports.userlist = async function (filters) {
    console.log('inside user list')
    if(filters.fields) filters.fields = filters.fields.split(',')
    const { fields = [], page = 0, limit = 10, filteringConditions = [], q:search = '', sort = 'id' } = filters;
    const offset = (page && page > 0 ? page - 1 : 0) * limit
    const sortOrder = sort[0] === '-' ? [sort.slice(1), 'desc'] : [sort, 'asc']
    
    return await knex('users')
        .select(fields)
        .limit(limit)
        .offset(offset)
        .where(builder => {
            if(search) {
                searchableFields.forEach(fieldName => {
                    builder.orWhere(fieldName, 'like', `%${search}%`)
                })
            }
        })
        // filters not working atm
        .where(builder => {
            filteringConditions.forEach(condition => {
                builder.where(...condition)
            });
        })
        .orderBy(...sortOrder);
}

module.exports.user = async function (id) {
    console.log('inside user info')
    return await knex('users').where('id', id);
}