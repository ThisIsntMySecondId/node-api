const bcrypt = require('bcrypt');
const db = require('../../db/knex')

// TODO: add history to store calls
// TODO: give user ability to generate different api keys with different scopes (permissions)
// TODO: do not count the errors of 400, 404, 402, 500

module.exports = async function (req, res, next) {
    console.log("in auth middleeware")
    try {
        if (!('API_KEY' in req.query)) {
            return res.status(400).json({
                message: 'API_KEY is required as query parmeter'
            });
        }

        const apiUser = await db('api_users').where('api_key', '=', req.query.API_KEY)
        if (!apiUser.length) {
            return res.status(401).json({
                message: 'unauthorized user'
            });
            // TODO Beautify this error in future
        }

        req.apiUser = apiUser[0];
        // console.log("req.apiUser", req.apiUser)
        if(req.apiUser.api_call_limit <= req.apiUser.api_call_count) {
            return res.status(402).json({
                message: "You have excceeded your call limit"
            })
        }

        await db('api_users').where('id', req.apiUser.id).increment('api_call_count', 1);
        
        next();
    } catch (error) {
        res.status(400).json(error)
    }
}